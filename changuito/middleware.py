from proxy import CartProxy


class CartMiddleware(object):

    # Original Code
    # def process_request(self, request):
    # 	request.cart = CartProxy(request)

    # Modified for LiPAD
    # Stop generating download carts for anonymous users
    def process_request(self, request):
    	if request.user is not 'AnonymousUser':
	        request.cart = CartProxy(request)

